﻿namespace Hola_Mundo_Version_Shinobu
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnHola = new System.Windows.Forms.Button();
            this.btnAdios = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbxAdios = new System.Windows.Forms.PictureBox();
            this.pbxHola = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxAdios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHola)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 194F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnHola, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnAdios, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(678, 405);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Hola_Mundo_Version_Shinobu.Properties.Resources.shinobu_icon;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(188, 212);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnHola
            // 
            this.btnHola.Location = new System.Drawing.Point(3, 221);
            this.btnHola.Name = "btnHola";
            this.btnHola.Size = new System.Drawing.Size(188, 84);
            this.btnHola.TabIndex = 1;
            this.btnHola.Text = "HOLA";
            this.btnHola.UseVisualStyleBackColor = true;
            this.btnHola.Click += new System.EventHandler(this.btnHola_Click);
            // 
            // btnAdios
            // 
            this.btnAdios.Location = new System.Drawing.Point(3, 313);
            this.btnAdios.Name = "btnAdios";
            this.btnAdios.Size = new System.Drawing.Size(188, 89);
            this.btnAdios.TabIndex = 2;
            this.btnAdios.Text = "ADIÓS";
            this.btnAdios.UseVisualStyleBackColor = true;
            this.btnAdios.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pbxAdios);
            this.panel1.Controls.Add(this.pbxHola);
            this.panel1.Location = new System.Drawing.Point(197, 3);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 3);
            this.panel1.Size = new System.Drawing.Size(478, 399);
            this.panel1.TabIndex = 3;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pbxAdios
            // 
            this.pbxAdios.Image = global::Hola_Mundo_Version_Shinobu.Properties.Resources.shinobu_adios_4;
            this.pbxAdios.Location = new System.Drawing.Point(242, 21);
            this.pbxAdios.Name = "pbxAdios";
            this.pbxAdios.Size = new System.Drawing.Size(208, 357);
            this.pbxAdios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxAdios.TabIndex = 1;
            this.pbxAdios.TabStop = false;
            this.pbxAdios.Visible = false;
            // 
            // pbxHola
            // 
            this.pbxHola.Image = global::Hola_Mundo_Version_Shinobu.Properties.Resources.Shinobu_Hola;
            this.pbxHola.Location = new System.Drawing.Point(14, 21);
            this.pbxHola.Name = "pbxHola";
            this.pbxHola.Size = new System.Drawing.Size(183, 357);
            this.pbxHola.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHola.TabIndex = 0;
            this.pbxHola.TabStop = false;
            this.pbxHola.Visible = false;
            this.pbxHola.Click += new System.EventHandler(this.pictureBox2_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = " Shinobu";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxAdios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHola)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnHola;
        private System.Windows.Forms.Button btnAdios;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbxAdios;
        private System.Windows.Forms.PictureBox pbxHola;
    }
}

